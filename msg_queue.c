/// @file msg_queue.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione delle MESSAGE QUEUE.


#include <stdio.h>
#include <errno.h>
#include <sys/msg.h>
#include "defines.h"
#include "err_exit.h"
#include "msg_queue.h"

msq msqCreateNew() {
    msq id = msgget(IPC_PRIVATE, S_IRUSR | S_IWUSR);
    if (id fails)
        perrExit("Unable to create new message queue");
    return id;
}

msq msqCreate(key_t msqKey) {
    msq id = msgget(msqKey, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (id fails)
        perrExit("Unable to create message queue");
    return id;
}

msq msqGet(key_t msqKey) {
    msq id = msgget(msqKey, S_IRUSR | S_IWUSR);
    if (id fails)
        perrExit("Unable to get message queue");
    return id;
}

void msgSend(msq queue, const void *in, size_t msgsize) {
    if (msgsnd(queue, in, msgsize - sizeof(long), 0) fails)
        perrExit("Unable to send message on queue %d", queue);
}

ssize_t msgReceive(msq queue, void *out, size_t msgsize, long type, char no_wait) {
    ssize_t ret = msgrcv(queue, out, msgsize - sizeof(long), type, MSG_NOERROR | (no_wait? IPC_NOWAIT : 0));
    if (ret < 0 && (!no_wait || errno != ENOMSG) && errno != EINTR)
        perrExit("Unable to receive message %ld on queue %d", type, queue);
    return ret;
}

void msqRemove(msq queue) {
    if (msgctl(queue, IPC_RMID, NULL) fails)
        perrExit("Unable to remove message queue %d", queue);
}
