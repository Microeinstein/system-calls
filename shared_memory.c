/// @file shared_memory.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione della MEMORIA CONDIVISA.

#include <sys/stat.h>
#include <sys/shm.h>
#include "defines.h"
#include "err_exit.h"
#include "shared_memory.h"

//get, or create, a shared memory segment
shm shmCreate(key_t shmKey, int size) {
    shm shmid = shmget(shmKey, size, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (shmid fails)
        perrExit("Unable to allocate shared memory");
    return shmid;
}

//attach the shared memory
shmPtr shmAttach(shm shmid, int shmflg) {
    shmPtr ptr = shmat(shmid, 0, shmflg);
    if ((long)ptr fails)
        perrExit("Unable to attach to shared memory");
    return ptr;
}

//detach the shared memory segment
void shmDetach(shmPtr ptr) {
    if (shmdt(ptr) fails)
        perrExit("Unable to detach shared memory");
}

//remove the shared memory segment
void shmRemove(shm shmid) {
    if (shmctl(shmid, IPC_RMID, 0) fails)
        perrExit("Unable to remove shared memory");
}
