/// @file device.h
/// @brief Contiene le definizioni di DEVICE.

#pragma once
#include "defines.h"
#include "server.h"


typedef struct {
    int id;
    pid_t pid;
    Position pos;
    DISPOSABLE_ARR(char, fifo_path, PATH_LEN);
    DISPOSABLE(int, fd_fifo);
    DISPOSABLE(List, messages);
} DeviceGlobals;


void mainDevice(int id, Position pos);
