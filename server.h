/// @file server.c
/// @brief Contiene le definizioni del SERVER.

#pragma once
#include <unistd.h>
#include <sys/types.h>
#include "err_exit.h"
#include "defines.h"
#include "msg_queue.h"

#define AMOUNT_SHM 2
#define AMOUNT_DEVICES 5 //5
#define AMOUNT_CHILDRENS (AMOUNT_DEVICES + 1)
#define AMOUNT_SEMAPHORES (AMOUNT_SHM + 2) // = [board, ack, ...]
#define FILE_POSIZIONI_DEV 4
// = (BOARD_SIZE_LOG*2 + 1) + 1     "x,y|"
#define FILE_POSIZIONI_ROW 19
// = (BOARD_SIZE_LOG*2 + 1) * AMOUNT_DEVICES + (AMOUNT_DEVICES - 1)
#define MOVE_INTERVALL 2

typedef struct {
    ushort x, y;
} Position;

typedef struct {
    long id;
    Position pos;
} PositionMsg;

typedef struct {
    pid_t server_pid;
    pid_t device_pid[AMOUNT_DEVICES];
    key_t msg_queue_key;
    char terminating;
    char moving;
    int move_step;
    Semaphore SEM_IDX_MOVE;
    DISPOSABLE(int,     fd_file_posizioni);
    DISPOSABLE(msq,     queue_posizioni);
    DISPOSABLE(msq,     queue_ack_sync);
    DISPOSABLE(Board,   board);
    DISPOSABLE(AckList, acklist);
    DISPOSABLE(semSet,  semaphores);
} ServerGlobals;

//copy of this table is shared among childrens
extern ServerGlobals srvGlobals;
#define A srvGlobals.acklist
#define B srvGlobals.board
