/// @file device.c
/// @brief Contiene l'implementazione di DEVICE.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <math.h>
#include "defines.h"
#include "device.h"
#include "server.h"
#include "fifo.h"
#include "signals.h"
#include "msg_queue.h"


static DeviceGlobals devGlobals;
#define G devGlobals

#define INITSET(name)        _INITSET(devGlobals, name)
#define INITVAL(name)        _INITVAL(devGlobals, name)
#define INITPTR(name)        _INITPTR(devGlobals, name)
#define CLEANVAL(name, func) _CLEANVAL(devGlobals, name, func)
#define CLEANPTR(name, func) _CLEANPTR(devGlobals, name, func)


static char readMessage(Message *out) {
    ssize_t amount = read(G.fd_fifo, out, MSGSIZE);
    if (amount == 0 || (amount fails && errno == EAGAIN)) //EOF or not opened for writing already
        return 0;
    if (amount < MSGSIZE)
        errExit("Incomplete message read from fifo.\n", NULL);

    char is_probe = (out->max_distance == 0);
    debugp("<d%d> probe: %d\n", G.pid, is_probe);
    if (is_probe) {
        debugp("<d%d> locking acklist...\n", G.pid);
        semP(&A.sem);

        List l = {};
        debugp("<d%d> checking availability...\n", G.pid);
        char exists = ackIterAcks(&A, out->message_id, &l);
        clearList(&l);

        debugp("<d%d> getting client queue...\n", G.pid);
        msq queue = msqGet(srvGlobals.msg_queue_key);
        ProbeMsg response = {
            .id = (out->pid_sender | MSG_PID_FLAG),
            .available = !exists
        };
        debugp("<d%d> sending response (%d)...\n", G.pid, !exists);
        msgSend(queue, &response, sizeof(ProbeMsg));

        debugp("<d%d> unlocking acklist...\n", G.pid);
        semV(&A.sem);
        return 0;
    }

    Acknowledgment ack = {
        .pid_sender   = out->pid_sender,
        .pid_receiver = G.pid,
        .message_id   = out->message_id,
        .timestamp    = now()
    };
    debugp("<d%d> locking acklist...\n", G.pid);
    semP(&A.sem);
    debugp("<d%d> pushing %d: p%d -> p%d\n", G.pid, ack.message_id, ack.pid_sender, ack.pid_receiver);
    ackPush(&A, &ack);
    debugp("<d%d> unlocking acklist...\n", G.pid);
    semV(&A.sem);

    return 1;
}
static char sendMessage(Message *in) {
    Position *p = &G.pos;
    List l = {};
    int received_by = 1; //itself
    char already_received = 0;
    char sent = 0;

    semP(&B.sem);
    for (int y=0; y<BOARD_SIZE && !sent; y++)
    for (int x=0; x<BOARD_SIZE && !sent; x++) {
        pid_t cell = (*B.pids)[y][x];
        if (cell == 0 || cell == G.pid)
            continue;
        debugp("<d%d> found p%d at x%d y%d\n", G.pid, cell, x, y);

        already_received = 0;
        debugp("<d%d> locking acklist...\n", G.pid);
        semP(&A.sem);
        while (ackIterAcks(&A, in->message_id, &l)) {
            Acknowledgment *ack = &(A.mem->acks)[*(int*)l.cur->value];
            if (ack->pid_receiver != cell) //another device
                continue;
            already_received = 1;
            break;
        }
        while (iterateToEnd(&l, 1));
        debugp("<d%d> unlocking acklist...\n", G.pid);
        semV(&A.sem);
        if (already_received) {
            received_by++;
            debugp("<d%d> already received by device\n", G.pid);
            continue;
        }

        int dx = x - p->x,
            dy = y - p->y;
        char inside = (sqrt((dx*dx) + (dy*dy)) <= in->max_distance);
        if (!inside) {
            debugp("<d%d> not inside radius\n", G.pid);
            continue;
        }

        sendMessageExternal(in, G.pid, cell, 1);
        sent = 1;
    }
    semV(&B.sem);

    debugp("<d%d> received by %d device(s)\n", G.pid, received_by);
    if (received_by >= AMOUNT_DEVICES)
        return 2;
    return sent;
}

static void readMessages(List *msgs) {
    Entry *last = lastList(msgs);
    Message msg;
    debugp("<d%d> reading messages...\n", G.pid);
    while (readMessage(&msg)) {
        debugp("<d%d> new message: \"%s\"\n", G.pid, msg.message);
        last = appendList(msgs, last, MSGSIZE);
        *(Message*)last->value = msg;
    }
    debugp("<d%d> list.first: %p\n", G.pid, msgs->first);
    msgs->cur = NULL;
}
static void sendMessages(List *msgs) {
    List toRemove = {};
    Entry *e;

    Entry *last = NULL;
    debugp("<d%d> sending messages...\n", G.pid);
    while (iterateList(msgs, 0)) {
        e = msgs->cur;
        debugp("<d%d> message \"%s\"\n", G.pid, ((Message*)e->value)->message);
        char status = sendMessage(e->value);
        if (status) {
            if (status == 2) {
                debugp("<d%d> received by every device\n", G.pid);
                DummyMsg sync = {
                    .id = ((Message*)e->value)->message_id
                };
                msgSend(srvGlobals.queue_ack_sync, &sync, sizeof(DummyMsg));
            }
            last = appendList(&toRemove, last, 0);
            last->value = e;
            debugp("<d%d> success\n", G.pid);
        } else
            debugp("<d%d> not sent\n", G.pid);
    }

    Entry *prev;
    if (toRemove.first) {
        debugp("<d%d> removing messages...\n", G.pid);
        while (iterateList(&toRemove, 1)) { //iterate, clear after
            prev = NULL;
            while (iterateList(msgs, 0)) { //iterate, do not clear after
                e = msgs->cur;
                if (e == toRemove.cur->value) { //is message the same that must be removed now?
                    debugp("<d%d> removing %d\n", G.pid, ((Message*)e->value)->message_id);
                    removeEntry(msgs, prev, e);
                    break;
                }
                prev = e;
            }
            msgs->cur = NULL;
        }
    }
}

static void disposeGlobals() {
    CLEANVAL(fd_fifo, closeFile);
    CLEANVAL(fifo_path, deleteFile);
    CLEANPTR(messages, clearList);
}
static void setDevicePos() {
    PositionMsg msg;
    ssize_t amount = msgReceive(srvGlobals.queue_posizioni, &msg, sizeof(PositionMsg), G.id+1, 0);
    if (amount < 0)
        return;

    semP(&B.sem);
    (*B.pids)[G.pos.y][G.pos.x] = 0;
    (*B.pids)[msg.pos.y][msg.pos.x] = G.pid;
    G.pos = msg.pos;
    printf("%d %d %d msgs:", G.pid, G.pos.x, G.pos.y);
    while (iterateList(&G.messages, 0))
        printf(" %d", ((Message*)G.messages.cur->value)->message_id);
    printf("\n");
    semV(&srvGlobals.SEM_IDX_MOVE);
    semV(&B.sem);
}
static void signalsHandler(sig signal) {
    switch (signal) {
        case SIGTERM:
            debugp("<d%d> received SIGTERM.\n", G.pid);
            //disposeGlobals();
            exit(EXIT_SUCCESS);
            break;
        case SIGUSR1: //device interrupted from sleeping or waiting for message from queue
            break;
    }
}

void mainDevice(int id, Position pos) {
    atExit(&disposeGlobals);
    G.id = id;
    G.pid = getpid();
    debugp("<d%d> initializing...\n", G.pid);
    debugp("<d%d> board semaphore set: %d\n", G.pid, B.sem.set);
    semP(&B.sem);
    (*B.pids)[pos.y][pos.x] = G.pid;
    semV(&B.sem);
    G.pos = pos;

    sigset_t sigset;
    sigFill(&sigset);
    sigExclude(&sigset, SIGTERM);
    sigExclude(&sigset, SIGUSR1);
    sigSetMask(&sigset);
    sigSetHandler(SIGTERM, &signalsHandler);
    sigSetHandler(SIGUSR1, &signalsHandler);

    getFIFOPath(G.fifo_path, G.pid);
    makeFIFO(INITVAL(fifo_path));
    INITSET(fd_fifo) = openRead(G.fifo_path, 1); //non-blocking
    debugp("<d%d> ready\n", G.pid);

    List *msgs = &G.messages;
    while (1) {
        if (msgs->first)
            sendMessages(msgs);
        readMessages(msgs);
        setDevicePos();
    }
}
