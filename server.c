/// @file server.c
/// @brief Contiene l'implementazione del SERVER.

//#define DO_TESTS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <signal.h>

#include "err_exit.h"
#include "defines.h"

#include "shared_memory.h"
#include "semaphore.h"
#include "signals.h"
#include "fifo.h"
#include "msg_queue.h"

#include "server.h"
#include "device.h"
#include "ack_manager.h"


ServerGlobals srvGlobals; //.DATA -> all zeros
#define G srvGlobals

#define INITSET(name)        _INITSET(srvGlobals, name)
#define INITVAL(name)        _INITVAL(srvGlobals, name)
#define INITPTR(name)        _INITPTR(srvGlobals, name)
#define CLEANVAL(name, func) _CLEANVAL(srvGlobals, name, func)
#define CLEANPTR(name, func) _CLEANPTR(srvGlobals, name, func)


static void showHelp() {
    printf(
"Usage: server <msg_queue_key> <file_posizioni>\n"
"\n"
    );
}

static char readBufferChar(string buff, int *i) {
    char c = buff[*i];
    *i += 1;
    return c;
}
static void readBufferStr(string txt, string buff, int *i, int size) {
    memcpy(txt, buff+(*i), size);
    *i += size;
}
static void readNextPositions(int fd, Position pos[AMOUNT_DEVICES]) {
    rewindIfEof(fd); // To handle last newline (unix standard)
    char buffer[FILE_POSIZIONI_ROW];
    char newline;
    int line = currentOffset(fd) / (FILE_POSIZIONI_ROW+1) + 1;
    ssize_t amount = read(fd, buffer, FILE_POSIZIONI_ROW);
    ssize_t nl = read(fd, &newline, 1);
    if (amount < FILE_POSIZIONI_ROW || (nl && newline != '\n'))
        errExit("Incomplete positions row at line %d.\n", NULL, line);
    // assert nl==0 || (nl==1 && newline=='\n')
#define DIGITS_PLUS_SEP (BOARD_SIZE_LOG+1)
    char txt[DIGITS_PLUS_SEP] = {}; // "x\0"
    int bo = 0;
    Position p;
    for (int d=0; d<AMOUNT_DEVICES; d++) {
        if (d > 0) {
            if (readBufferChar(buffer, &bo) != '|')
                errExit("Invalid column separator at line %d.\n", NULL, line);
        }
        readBufferStr(txt, buffer, &bo, BOARD_SIZE_LOG);
        p.x = toInt(txt);
        if (readBufferChar(buffer, &bo) != ',')
            errExit("Invalid coordinates separator at line %d.\n", NULL, line);
        readBufferStr(txt, buffer, &bo, BOARD_SIZE_LOG);
        p.y = toInt(txt);
        pos[d] = p;
    }
    if (!nl) //EOF
        rewindFile(fd);
}
static void moveDevices() {
    alarm(MOVE_INTERVALL);
    if (G.moving) return;
    G.moving = 1;

    Position pos[AMOUNT_DEVICES];
    readNextPositions(G.fd_file_posizioni, pos);

    //semP(&B.sem);
    G.move_step++;
    printf("# Step %05d: device positions ###############\n", G.move_step);
    //pid_t zero[BOARD_SIZE][BOARD_SIZE] = {0};
    //memcpy(*B.pids, zero, sizeof(zero));
    for (int d=0; d<AMOUNT_DEVICES; d++) {
        PositionMsg msg = {
            .id  = d+1,
            .pos = pos[d]
        };
        msgSend(G.queue_posizioni, &msg, sizeof(PositionMsg));
        //pid_t dpid = G.device_pid[d];
        //kill(dpid, SIGUSR1);
        semP(&G.SEM_IDX_MOVE);
    }
    printf("##############################################\n\n");
    //semV(&B.sem);

    G.moving = 0;
}

static void terminate() {
    //debugp("terminate()\n");
    if (G.terminating) return;
    G.terminating = 1;
    kill(0, SIGTERM);
}
static void waitForChildrens() {
    pid_t terminated;
    char has_childrens = 1;
    do {
        //debugp("Waiting...\n");
        terminated = wait(NULL);
        //debugp("Wait stop (%d)\n", terminated);
        if (terminated fails) {
            has_childrens = (errno != ECHILD);
            if (has_childrens)
                perrExit("Unable to wait for children");
        }
        if (!G.terminating)
            errExit("Unexpected child termination: %d\n", NULL, terminated);
    } while (has_childrens);
}
static void signalsHandler(sig signal) {
    //debugp("signal %d\n", signal);
    switch (signal) {
        case SIGTERM:
            debugp("<server> received SIGTERM.\n");
            terminate();
            break;
        case SIGALRM:
            //debugp("Alarm received\n");
            moveDevices();
            break;
        default:
            debugp("<server> received unknown signal.\n");
            break;
    }
}

// BEGIN Pulizia risorse
static void disposeGlobals() {
    if (getpid() != G.server_pid)
        return;
    CLEANVAL(fd_file_posizioni, closeFile);
    CLEANVAL(queue_posizioni, msqRemove);
    CLEANVAL(queue_ack_sync, msqRemove);
    CLEANPTR(board, disposeBoard);
    CLEANVAL(semaphores, semRemoveSet);
}
// END

// BEGIN Funzioni test
#ifdef DO_TESTS
void __test_FilePosizioni(int fd, Position pos[AMOUNT_DEVICES]) {
    for (int r=0; r<10; r++) {
        readNextPositions(fd, pos);
        for (int d=0; d<AMOUNT_DEVICES; d++) {
            if (d) printf("   ");
            printf("%d_%d", pos[d].x, pos[d].y);
        }
        printf("\n");
    }
}
void __test_Board(Board *b) {
    for (int r=0; r<BOARD_SIZE; r++) {
        for (int c=0; c<BOARD_SIZE; c++) {
            //(*b->pids)[r][c] = 7;
            printf("%d ", (*b->pids)[r][c]);
        }
        printf("\n");
    }
}
void __test_AckList(AckList *a) {
    for (int id=1; id<=5; id++)
    for (int n=0; n<id*2; n++) {
        Acknowledgment ack = {
            .pid_sender   = 2,
            .pid_receiver = 3,
            .message_id   = id,
            .timestamp    = 0
        };
        ackPush(a, &ack);
    }
    List l = {};

    while (ackIterMessages(a, &l))
        printf("%p %p %d\n", l.first, l.cur, *(int*)l.cur->value);
    printf("%p %p\n\n", l.first, l.cur);

    // 1 1, 2 2 2 2, 3 3 3 3 3 3, ...
    while (ackIterAcks(a, 3, &l))
        printf("%p %p %d\n", l.first, l.cur, *(int*)l.cur->value);
    printf("%p %p\n\n", l.first, l.cur);
}
#endif
// END


int main(int argc, char * argv[]) {
    if (argc < 2) errExit("Missing argument: msg_queue_key\n", &showHelp);
    if (argc < 3) errExit("Missing argument: file_posizioni\n", &showHelp);
    if (argc > 3) errExit("Too many arguments.\n", &showHelp);

    G.server_pid = getpid();
    atExit(&disposeGlobals);

    // Inizializza message queue e file posizioni
    G.msg_queue_key = toInt(argv[1]);
    INITSET(fd_file_posizioni) = openRead(argv[2], 0);
    Position pos[AMOUNT_DEVICES];
#ifdef DO_TESTS
    __test_FilePosizioni(G.fd_file_posizioni, pos);
#endif

    // Inizializza semafori
    INITSET(semaphores) = semCreateSet(AMOUNT_SEMAPHORES);
    debugp("<server> semaphore set: %d\n", G.semaphores);
    semVal semValues[AMOUNT_SEMAPHORES] = {1,1,0};
    semInit(G.semaphores, semValues);
    Semaphore SEM_IDX_BOARD = { .set = G.semaphores, .num = 0 };
    Semaphore SEM_IDX_ACK   = { .set = G.semaphores, .num = 1 };
    Semaphore SEM_IDX_MOVE  = { .set = G.semaphores, .num = 2 };
    G.SEM_IDX_MOVE = SEM_IDX_MOVE;

    // Inizializza message queue posizioni e sincronizzazione pulizia acks
    INITSET(queue_posizioni) = msqCreateNew();
    INITSET(queue_ack_sync) = msqCreateNew();

    // Inizializza board e acklist
    createBoard(SEM_IDX_BOARD, INITPTR(board));
    createAckList(SEM_IDX_ACK, INITPTR(acklist));
    debugp("<server> board semaphore set: %d\n", B.sem.set);
    initBoard(&B);
    initAckList(&A);
#ifdef DO_TESTS
    __test_Board(&B);
    __test_AckList(&A);
    return EXIT_SUCCESS;
#endif

    pid_t pid;

    // Creazione Ack Manager
    pid = fork();
    if (pid fails)
        perrExit("Unable to fork for the ack_manager");
    if (pid == 0) { //ackmanager
        mainAckManager(G.msg_queue_key);
        return EXIT_SUCCESS;
    }

    // Creazione devices
    readNextPositions(G.fd_file_posizioni, pos);
    for (int d=0; d<AMOUNT_DEVICES; d++) {
        pid = fork();
        if (pid fails)
            perrExit("Unable to fork for a new device");
        if (pid == 0) { //device
            mainDevice(d, pos[d]);
            return EXIT_SUCCESS;
        }
        debugp("<server> new device: %d\n", pid);
        G.device_pid[d] = pid;
    }

    sigset_t sigset;
    sigFill(&sigset);
    sigExclude(&sigset, SIGTERM);
    sigExclude(&sigset, SIGCHLD); //required for wait()
    sigExclude(&sigset, SIGALRM); //required for alarm()
    sigSetMask(&sigset);
    sigSetHandler(SIGTERM, &signalsHandler);
    sigSetHandler(SIGALRM, &signalsHandler);
    atExit(&terminate);
    alarm(MOVE_INTERVALL);
    //printf("Running...\n");

    waitForChildrens();
    return EXIT_SUCCESS;
}
