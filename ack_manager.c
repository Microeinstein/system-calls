/// @file ack_manager.c
/// @brief Contiene l'implementazione di ACK_MANAGER.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/msg.h>
#include "err_exit.h"
#include "defines.h"
#include "signals.h"
#include "msg_queue.h"
#include "server.h"
#include "ack_manager.h"


AckManagerGlobals ackGlobals;
#define G ackGlobals

#define INITSET(name)        _INITSET(ackGlobals, name)
#define INITVAL(name)        _INITVAL(ackGlobals, name)
#define INITPTR(name)        _INITPTR(ackGlobals, name)
#define CLEANVAL(name, func) _CLEANVAL(ackGlobals, name, func)
#define CLEANPTR(name, func) _CLEANPTR(ackGlobals, name, func)


static void checkAcks() {
    alarm(CHECK_INTERVALL);
    if (G.checking) return;
    debugp("<ackManager> checking...\n");
    G.checking = 1;

    List l1 = {};
    List l2 = {};

    semP(&A.sem);
    debugp("<ackManager> current acks: %d\n", A.mem->occupied);
    while (ackIterMessages(&A, &l1)) {
        int message_id = *(int*)l1.cur->value;
        debugp("<ackManager> message %d...\n", message_id);

        int received = 0;
        while (ackIterAcks(&A, message_id, &l2))
            received++;
        if (received < AMOUNT_DEVICES)
            continue;
        debugp("<ackManager> received by every device\n");

        while (ackIterAcks(&A, message_id, &l2)) {
            int slot = *(int*)l2.cur->value;
            Acknowledgment *ack = &(A.mem->acks)[slot];
            AcknowledgmentMsg msg = {
                .id  = message_id,
                .ack = *ack
            };
            debugp("<ackManager> sending ack p%d...\n", ack->pid_sender);
            msgSend(G.queue, &msg, sizeof(AcknowledgmentMsg));
        }
        AcknowledgmentMsg msg = {
            .id  = message_id,
            .ack = {}
        };
        debugp("<ackManager> sending terminator...\n");
        msgSend(G.queue, &msg, sizeof(AcknowledgmentMsg)); //terminator message
        semV(&A.sem);

        DummyMsg dummy;
        msgReceive(srvGlobals.queue_ack_sync, &dummy, sizeof(DummyMsg), message_id, 0);

        semP(&A.sem);
        debugp("<ackManager> freeing ack slots...\n");
        while (ackIterAcks(&A, message_id, &l2)) {
            ackPop(&A, *(int*)l2.cur->value);
            //Acknowledgment* ack = ;
            //ack->pid_sender = 0;
            //if (!MSGSLOT_IS_FREE(ack))
            //    errExit("Acknowledgment slot is not free.", NULL);
        }
    }
    semV(&A.sem);

    G.checking = 0;
}

static void disposeGlobals() {
    CLEANVAL(queue, msqRemove);
}
static void signalsHandler(sig signal) {
    //printf("signal %d\n", signal);
    switch (signal) {
        case SIGTERM:
            debugp("<ackManager> received SIGTERM.\n");
            exit(EXIT_SUCCESS);
            break;
        case SIGALRM:
            checkAcks();
            break;
    }
}

void mainAckManager(key_t msg_queue_key) {
    debugp("<ackManager> initializing...\n");
    atExit(&disposeGlobals);
    sigset_t sigset;
    sigFill(&sigset);
    sigExclude(&sigset, SIGTERM);
    sigExclude(&sigset, SIGALRM); //required for alarm()
    sigSetMask(&sigset);
    sigSetHandler(SIGTERM, &signalsHandler);
    sigSetHandler(SIGALRM, &signalsHandler);

    INITSET(queue) = msqCreate(msg_queue_key);

    alarm(CHECK_INTERVALL);
    debugp("<ackManager> initialized\n");
    while (1)
        pause();
}
