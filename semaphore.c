/// @file semaphore.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione dei SEMAFORI.

#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include "semaphore.h"
#include "err_exit.h"

//create a semaphore set
semSet semCreateSet(int size) {
    semSet set = semget(IPC_PRIVATE, size, IPC_CREAT | S_IRWXU);
    if (set fails)
        perrExit("Unable to create semaphore set");
    return set;
}

//initialize a semaphore set
void semInit(semSet set, semVal *array) {
    semun data;
    data.array = array;
    if (semctl(set, 0, SETALL, data) fails)
        perrExit("Unable to initialize semaphore set");
}

//execute an operation on a single semaphore of the set
void semExec(semSet set, semNum num, semOp op) {
    char interrupted;
    struct sembuf sop = {
        .sem_num = num,
        .sem_op  = op,
        .sem_flg = 0
    };
    struct sembuf sop_undo = {
        .sem_num = num,
        .sem_op  = -op,
        .sem_flg = IPC_NOWAIT
    };
    do {
        debugp("<?> semset%d[%d]: %d (%+d)\n", set, num, semctl(set, num, GETVAL, 0), op);
        int status = semop(set, &sop, 1);
        interrupted = status fails && (errno == EINTR);
        if (interrupted) {
            semop(set, &sop_undo, 1);
            debugp("<?> SIGNAL semset%d[%d]: %d\n", set, num, semctl(set, num, GETVAL, 0));
        } else if (status fails)
            perrExit("Unable to execute operation %+d on semaphore %d[%d]", op, set, num);
    } while (interrupted);
}

//remove a semaphore set
void semRemoveSet(semSet set) {
    if (semctl(set, 0, IPC_RMID, 0) fails)
        perrExit("Unable to remove semaphore set");
}

//lock a semaphore
void semP(Semaphore *s) {
    //debugp("<?> lock semaphore: %d[%d]\n", s->set, s->num);
    semExec(s->set, s->num, -1);
}

//unlock a semaphore
void semV(Semaphore *s) {
    semExec(s->set, s->num, 1);
}
