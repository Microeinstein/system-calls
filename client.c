/// @file client.c
/// @brief Contiene l'implementazione del client.

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include "err_exit.h"
#include "defines.h"
#include "msg_queue.h"


msq queue;
pid_t self;


static void showHelp() {
    printf(
"Usage: client <msg_queue_key>\n"
"\n"
    );
}

static void gotoNextInputLine() {
    char c;
    while ((c = getchar()) != '\n' && c != EOF);
}
static void askInt(const string prompt, int *out) {
    printf("<client> %s: ", prompt);
    scanf(" %d", out);
    gotoNextInputLine();
}
static void askString(const string prompt, string out, int size) {
    printf("<client> %s: ", prompt);
    string res = fgets(out, size, stdin);
    if (res != out)
        errExit("Unable to get user string.", NULL);
}
static void askDouble(const string prompt, double *out) {
    printf("<client> %s: ", prompt);
    scanf(" %lf", out);
    gotoNextInputLine();
}
static void printTo(int fd, const string fmt, ...) {
    va_list args;
    va_start(args, fmt);
    char lineBuf[4096] = {};
    vsprintf(lineBuf, fmt, args);
    int len = strlen(lineBuf);
    va_end(args);

    ssize_t amount = write(fd, lineBuf, len);
    if (amount < len)
        errExit("Incomplete message write to output file.\n", NULL);
}
static long ackMsgKey(Entry *e) {
    return ((AcknowledgmentMsg*)e->value)->ack.timestamp;
}

static void askMessageData(pid_t *device, Message *out) {
    char input_err = 0;
    do {
        askInt("device pid", device);
        input_err = !pidExists(*device);
        if (input_err)
            printf("<client> process does not exist.\n");
    } while (input_err);
    debugp("<client> pid=%d\n", *device);

    askString("message", out->message, MSGLEN);
    do {
        askDouble("max distance", &out->max_distance);
        input_err = out->max_distance <= 0;
        if (input_err)
            printf("<client> max distance must be >0.\n");
    } while (input_err);

    do {
        do {
            askInt("message id", &out->message_id);
            input_err = out->message_id <= 0;
            if (input_err)
                printf("<client> message id must be >0.\n");
        } while (input_err);
        Message probe = {
            .max_distance = 0,
            .message_id = out->message_id,
        };
        debugp("<client> asking for availability...\n");
        sendMessageExternal(&probe, self, *device, 1);
        ProbeMsg resp = {};
        debugp("<client> receiving response...\n");
        msgReceive(queue, &resp, sizeof(ProbeMsg), self | MSG_PID_FLAG, 0);
        input_err = !resp.available;
        if (input_err)
            printf("<client> this id is already in use.\n");
    } while (input_err);
}
static void writeOutputFile(Message *in, List *l) {
    char outpath[PATH_LEN] = {};
    getOutputPath(outpath, in->message_id);
    debugp("<client> creating output file...\n");

    int outfd = openWriteNew(outpath, 0);
    printTo(outfd, "Messaggio %d: %s\nLista acknowledgment:\n", in->message_id, in->message);
    while (iterateList(l, 0)) {
        AcknowledgmentMsg *am = (AcknowledgmentMsg*)l->cur->value;
        char stime[26] = {};
        printTime(stime, am->ack.timestamp);
        printTo(outfd, "%5d, %5d, %s\n", am->ack.pid_sender, am->ack.pid_receiver, stime);
    }
    closeFile(outfd);
}
static void sendMessage(pid_t *device, Message *in, List *out) {
    debugp("<client> sending...\n");
    sendMessageExternal(in, self, *device, 1);

    AcknowledgmentMsg am;
    char terminator;
    do {
        debugp("<client> awaiting message...\n");
        msgReceive(queue, &am, sizeof(AcknowledgmentMsg), in->message_id, 0);
        terminator = MSGSLOT_IS_FREE((&am.ack));
        debugp("<client> received ack p%d\n", am.ack.pid_sender);
        if (!terminator) {
            Entry *e = appendSortedList(out, sizeof(AcknowledgmentMsg), am.ack.timestamp, &ackMsgKey);
            *(AcknowledgmentMsg*)e->value = am;
        }
    } while (!terminator);
}

int main(int argc, char * argv[]) {
    if (argc < 2) errExit("Missing argument: msg_queue_key\n", &showHelp);

    key_t msg_queue_key = toInt(argv[1]);
    queue = msqGet(msg_queue_key);
    self = getpid();

    pid_t device;
    Message msg = {};
    List acks = {};
    askMessageData(&device, &msg);
    sendMessage(&device, &msg, &acks);
    writeOutputFile(&msg, &acks);

    debugp("<client> done\n");
    return EXIT_SUCCESS;
}
