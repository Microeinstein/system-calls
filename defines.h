/// @file defines.h
/// @brief Contiene la definizioni di variabili
///         e funzioni specifiche del progetto.

#pragma once

#define BUILD_BUG_ON(condition) ((void)sizeof(char[1 - 2*!!(condition)]))
#define PATH_LEN 100
#define COMMON_ERRCODE -1
//#define DEBUG_PRINT
#define fails == COMMON_ERRCODE
#define unreachable _exit(1)

#include <sys/types.h>
#include "shared_memory.h"
#include "semaphore.h"


typedef char *string;

typedef struct _Entry {
    int size;
    void *value;
    struct _Entry *next;
} Entry;

typedef struct {
    Entry *first, *cur;
} List;


#define MSGLEN 256
typedef struct {
    pid_t  pid_sender;
    pid_t  pid_receiver;
    int    message_id;
    char   message[MSGLEN];
    double max_distance;
} Message;
#define MSGSIZE sizeof(Message)

typedef struct {
    pid_t  pid_sender;
    pid_t  pid_receiver;
    int    message_id;
    time_t timestamp;
} Acknowledgment;
#define MSGSLOT_IS_FREE(ptr) (ptr->pid_sender == 0)


#define BOARD_SIZE 10
#define BOARD_MEM  (sizeof(pid_t) * BOARD_SIZE * BOARD_SIZE)
#define BOARD_SIZE_LOG 1 //ceiling(log10(BOARD_SIZE))
typedef struct {
    Semaphore sem;
    shm       memID;
    pid_t   (*pids)[BOARD_SIZE][BOARD_SIZE];
} Board;

#define ACKLIST_ENTRIES 100
typedef struct {
    Acknowledgment acks[ACKLIST_ENTRIES];
    int occupied;
} AckListMem;
#define ACKLIST_MEM sizeof(AckListMem) //(sizeof(Acknowledgment) * ACKLIST_ENTRIES)
typedef struct {
    Semaphore   sem;
    shm         memID;
    AckListMem *mem;
} AckList;


#define MSG_PID_FLAG (1L << 44)
typedef struct {
    long id;
    char available;
} ProbeMsg;

typedef struct {
    long id;
} DummyMsg;

typedef struct {
    long id;
    Acknowledgment ack;
} AcknowledgmentMsg;


//declare variable "name" and respective flag for disposal "clean_name"
#define DISPOSABLE(type, name) type name; char clean_##name

//declare array variable "name" and respective flag for disposal "clean_name"
#define DISPOSABLE_ARR(type, name, size) type name[size]; char clean_##name


//set flag for disposal and put "g.name" for subsequent value assignment
#define _INITSET(g, name) g.clean_##name = 1; g.name

//set flag for disposal and return the value of "g.name"
#define _INITVAL(g, name) (g.clean_##name = 1, g.name)

//set flag for disposal and return a pointer to "g.name"
#define _INITPTR(g, name) (g.clean_##name = 1, &g.name)


//clean variable "name" passing it by-value to "func",
//  only if the flag for disposal is set
#define _CLEANVAL(g, name, func) if (g.clean_##name) func(g.name); g.clean_##name = 0

//clean variable "name" passing it by-reference to "func",
//  only if the flag for disposal is set
#define _CLEANPTR(g, name, func) if (g.clean_##name) func(&g.name); g.clean_##name = 0


void __staticCheck();
void atExit(void (*function)());
int toInt(string txt);
time_t now();
void printTime(string out, time_t time);
char pidExists(pid_t pid);
void debugp(const string fmt, ...);
void string_backtrace(int depth);


int openRead(const string pathname, char nonblock);
int openWrite(const string pathname, char nonblock);
int openWriteNew(const string pathname, char nonblock);
void closeFile(int fd);
void deleteFile(const string pathname);
off_t currentOffset(int fd);
void rewindFile(int fd);
void rewindIfEof(int fd);
//int dupli(int fd);
//int dupli2(int from, int to);


void createBoard(Semaphore sem, Board *out);
void createAckList(Semaphore sem, AckList *out);
void initBoard(Board *b);
void initAckList(AckList *l);
void disposeBoard(Board *b);
void disposeAckList(AckList *l);

Entry *lastList(List *l);
Entry *appendList(List *l, Entry *last, int size);
void removeEntry(List *l, Entry *prev, Entry *e);
void clearList(List *l);
char iterateToEnd(List *l, char clear);
char iterateList(List *l, char clear);
Entry *appendSortedList(List *l, int size, long entryKey, long (*key)(Entry*));
void ackPush(AckList *l, Acknowledgment *ack);
void ackPop(AckList *l, int slot);
char ackIterMessages(AckList *al, List *l);
char ackIterAcks(AckList *al, int message_id, List *l);


void getFIFOPath(string out, pid_t pid);
void getOutputPath(string out, int msg_id);
void sendMessageExternal(Message *msg, pid_t sender, pid_t receiver, char ping);
