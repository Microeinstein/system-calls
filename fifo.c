/// @file fifo.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione delle FIFO.

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "defines.h"
#include "err_exit.h"
#include "fifo.h"

void makeFIFO(const string pathname) {
    if (mkfifo(pathname, O_CREAT | S_IRUSR | S_IWUSR) fails)
        perrExit("Unable to create FIFO '%s'", pathname);
}
