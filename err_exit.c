/// @file err_exit.c
/// @brief Contiene l'implementazione della funzione di stampa degli errori.

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "err_exit.h"


void perrExit(const string fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf(": %s\n", strerror(errno));
    exit(EXIT_FAILURE);
}

void errExit(const string fmt, void (*func)(void), ...) {
    va_list args;
    va_start(args, func);
    vprintf(fmt, args);
    va_end(args);
    if (func)
        func();
    exit(EXIT_FAILURE);
}
