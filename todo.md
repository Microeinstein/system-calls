* test
    * vari debugp
    * ~~[!] device lettura infinita~~
    * ~~[!] messaggio inviato da client non viene ricevuto~~
    * ~~[!] `iterateList` itera a partire dal secondo elemento~~
        -> `iterateList` + `iterateToEnd`
    * ~~[!] possibile race condition nello spostamento: contenuto lista messaggi~~
        -> spostamento sincrono
    * ~~[!] segmentation fault client+server (!?)~~
        -> formato printf
    * [?] client non riesce a scrivere su file di output (!?)
        * `Unable to get message queue: No such file or directory` (!!!?)
    * ~~[!] a `msgrcv` non piacciono i segnali~~
        -> rimozione SIGUSR1 SIGUSR2
    * ~~[!] l'ultimo device trova che tutti gli altri hanno già ricevuto il messaggio; non cancella~~
        -> `received_by`
    * ~~[!] messaggio non passa per tutti i device~~
    * ~~[!] pid_sender negli ack non sono pid~~
        -> _Lo erano_
    * ~~[!] ack manager non pulisce gli ack~~
    * ~~[!] client non scrive gli ack nel file di output~~
    * ~~[!] i messaggi continuano a girare tra i device (rimane nell'ultimo device)~~
        * ~~d4 → d5;  d5 segna ack;  ackmanager pulisce acklist;  d5 crede che nessuno l'abbia ricevuto;~~
        -> nuovo semaforo: cancella solo dopo che l'ultimo device ha tentato di inviare
