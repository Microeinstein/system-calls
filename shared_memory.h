/// @file shared_memory.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione della MEMORIA CONDIVISA.

#pragma once
#include <sys/shm.h>

#define SHM_RW 0

typedef int shm;
typedef void *shmPtr;

shm shmCreate(key_t shmKey, int size);
shmPtr shmAttach(shm shmid, int shmflg);
void shmDetach(shmPtr ptr);
void shmRemove(shm shmid);
