CFLAGS   := -Wall -std=gnu99 -g
INCLUDES := -I .
LIBS     := -lm
OBJDIR   := obj

# keep track of these files
KEEP_TRACK := Makefile

COMMON_SRCS := defines.c err_exit.c semaphore.c shared_memory.c msg_queue.c

SERVER_SRCS := $(COMMON_SRCS) fifo.c signals.c device.c ack_manager.c server.c
SERVER_OBJS := $(addprefix $(OBJDIR)/, $(SERVER_SRCS:.c=.o))

CLIENT_SRCS := $(COMMON_SRCS) client.c
CLIENT_OBJS := $(addprefix $(OBJDIR)/, $(CLIENT_SRCS:.c=.o))

# must be first rule
default: all


$(OBJDIR):
	@mkdir -p $(OBJDIR)

%.h:
	@echo "(missing header $@)"

$(OBJDIR)/%.o: %.c %.h $(KEEP_TRACK)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $< $(LIBS)

server: $(SERVER_OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $^ $(LIBS)
	@echo "Server compiled."
	@echo

client: $(CLIENT_OBJS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ $^ $(LIBS)
	@echo "Client compiled."
	@echo


run: server
	@./server 100 input/file_posizioni.txt

clean:
	@rm -vf $(SERVER_OBJS) $(CLIENT_OBJS)
	@rm -vf server
	@rm -vf client
	@rm -vf /tmp/dev_fifo.*
	@ipcrm -a
	@echo "Removed object files and executables..."

all: $(OBJDIR) server client

.PHONY: run clean
