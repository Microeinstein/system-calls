/// @file signals.h
/// @brief Contiene la definizioni di variabili
///         e funzioni specifiche dei SEGNALI.

#pragma once
#include <signal.h>
#include "err_exit.h"

typedef int sig;

void sigFill(sigset_t *set);
void sigExclude(sigset_t *set, sig num);
void sigSetMask(sigset_t *set);
void sigSetHandler(sig num, __sighandler_t handler);
