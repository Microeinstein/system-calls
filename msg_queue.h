/// @file msg_queue.c
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione delle MESSAGE QUEUE.

#pragma once
#include <sys/msg.h>

typedef int msq;

msq msqCreateNew();
msq msqCreate(key_t msqKey);
msq msqGet(key_t msqKey);
void msgSend(msq queue, const void *data, size_t size);
ssize_t msgReceive(msq queue, void *out, size_t msgsize, long type, char no_wait);
void msqRemove(msq queue);
