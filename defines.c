/// @file defines.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche del progetto.

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <execinfo.h>
#include "defines.h"
#include "err_exit.h"
#include "shared_memory.h"
#include "semaphore.h"


// BEGIN Generale
void __staticCheck() {
    //message write on FIFO must be atomic
    BUILD_BUG_ON(MSGSIZE > PIPE_BUF);
}
void atExit(void (*function)()) {
    if (atexit(function) < 0)
        perrExit("Unable to register atexit() function.");
}
int toInt(string txt) {
    return atoi(txt);
}
time_t now() {
    time_t t = time(NULL);
    if (t fails)
        perrExit("Unable to get current timestamp");
    return t;
}
void printTime(string out, time_t time) {
    struct tm *tm_info = localtime(&time);
    strftime(out, 26, "%F %T", tm_info); //"%Y-%m-%d %H:%M:%S"
}
char pidExists(pid_t pid) {
    if (!kill(pid, 0))
        return 1; //process found
    if (errno == ESRCH)
        return 0;
    perrExit("Unable to find process %d", pid);
    unreachable;
}
void string_backtrace(int depth) { //useless since most functions are static...
#ifdef DEBUG_PRINT
#define BT_BUF_SIZE 100
    if (depth < 0)
        depth = 0;
    void *trace[BT_BUF_SIZE] = {};
    int num = backtrace(trace, BT_BUF_SIZE);
    char **strings = backtrace_symbols(trace, num);
    printf("@ %s\n", strings[num - 1 - depth]);
    free(strings);
#endif
}
void debugp(const string fmt, ...) {
#ifdef DEBUG_PRINT
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
#endif
}
// END


// BEGIN Gestione file
int openRead(const string pathname, char nonblock) {
    int f = open(pathname, O_RDONLY | (nonblock? O_NONBLOCK : 0));
    if (f fails)
        perrExit("Unable to open file for reading '%s'", pathname);
    return f;
}
int openWrite(const string pathname, char nonblock) {
    int f = open(pathname, O_WRONLY | (nonblock? O_NONBLOCK : 0));
    if (f fails)
        perrExit("Unable to open file for writing '%s'", pathname);
    return f;
}
int openWriteNew(const string pathname, char nonblock) {
    int f = open(pathname, O_WRONLY | O_CREAT | O_TRUNC | (nonblock? O_NONBLOCK : 0));
    if (f fails)
        perrExit("Unable to create new file for writing '%s'", pathname);
    return f;
}
void closeFile(int fd) {
    if (close(fd) fails)
        perrExit("Unable to close file '%d'", fd);
}
void deleteFile(const string pathname) {
    if (unlink(pathname) fails)
        perrExit("Unable to delete file '%s'", pathname);
}
off_t currentOffset(int fd) {
    off_t offset = lseek(fd, 0, SEEK_CUR);
    if (offset fails)
        perrExit("Unable to get file offset");
    return offset;
}
void rewindFile(int fd) {
    if (lseek(fd, 0, SEEK_SET) fails)
        perrExit("Unable to rewind file descriptor");
}
void rewindIfEof(int fd) {
    char c;
    ssize_t ok = read(fd, &c, 1);
    if (!ok) //EOF
        rewindFile(fd);
    else if (lseek(fd, -1, SEEK_CUR) fails)
        perrExit("Unable to set file offset");
}
/*
int dupli(int fd) {
    int new = dup(fd);
    if (new fails)
        perrExit("Unable to duplicate file descriptor");
    return new;
}
int dupli2(int from, int to) {
    int new = dup2(from, to);
    if (new fails)
        perrExit("Unable to duplicate file descriptor");
    return new;
}
*/
// END


// BEGIN Gestione risorse
void createBoard(Semaphore sem, Board *out) {
    shm memoryID = shmCreate(IPC_PRIVATE, BOARD_MEM);
    shmPtr memory = shmAttach(memoryID, SHM_RW);
    //printf("%p\n", memory);
    Board tmp = {
        .sem    = sem,
        .memID  = memoryID,
        .pids   = memory
    };
    //printf("%p\n", tmp.pids);
    *out = tmp;
}
void createAckList(Semaphore sem, AckList *out) {
    shm memoryID = shmCreate(IPC_PRIVATE, ACKLIST_MEM);
    shmPtr memory = shmAttach(memoryID, SHM_RW);
    AckList tmp = {
        .sem      = sem,
        .memID    = memoryID,
        .mem      = memory
    };
    *out = tmp;
}
void initBoard(Board *b) {
    memset(b->pids, 0, BOARD_MEM);
}
void initAckList(AckList *l) {
    memset(l->mem, 0, ACKLIST_MEM);
}
void disposeBoard(Board *b) {
    //printf("%p\n", b->pids);
    shmDetach((shmPtr)b->pids);
    shmRemove(b->memID);
}
void disposeAckList(AckList *l) {
    shmDetach((shmPtr)l->mem);
    shmRemove(l->memID);
}
// END


// BEGIN Gestione liste e ACKs
Entry *lastList(List *l) {
    Entry *cur = l->first;
    Entry *last = NULL;
    while (cur) {
        last = cur;
        cur = cur->next;
    }
    return last;
}
//create new entry and link to previous ones
Entry *appendList(List *l, Entry *last, int size) {
    Entry *new = calloc(1, sizeof(Entry));
    new->size = size;
    new->value = size>0 ? calloc(1, size) : NULL;
    if (last)
        last->next = new;
    else
        l->first = new;
    return new;
}
void removeEntry(List *l, Entry *prev, Entry *e) {
    Entry *n = e->next;
    if (prev)
        prev->next = n;
    if (l && l->first == e)
        l->first = n;
    if (e->size > 0)
        free(e->value);
    free(e);
}
void clearList(List *l) {
    Entry *cur = l->first;
    Entry *prev = NULL;
    while (cur) {
        prev = cur;
        cur = cur->next;
        removeEntry(NULL, NULL, prev);
    }
    l->first = l->cur = NULL;
}
//if you want to use break while using iterators, put `while (iterateToEnd(&l));` !
char iterateToEnd(List *l, char clear) {
    if (l->cur)
        l->cur = l->cur->next;
    char end = (!l->cur); //end of list
    if (end) {
        if (clear)
            clearList(l);
        l->cur = NULL;
    }
    return !end;
}
char iterateList(List *l, char clear) {
    if (!l->cur) { //start of list
        l->cur = l->first;
        return !!l->cur; //if not NULL
    }
    return iterateToEnd(l, clear);
}
Entry *appendSortedList(List *l, int size, long entryKey, long (*key)(Entry*)) {
    Entry *new = calloc(1, sizeof(Entry));
    new->size = size;
    new->value = size>0 ? calloc(1, size) : NULL;

    Entry *prev = NULL;
    List iterlast = *l;
    iterlast.cur = NULL;
    while (iterateList(&iterlast, 0)) {
        if (key(iterlast.cur) > entryKey)
            break;
        prev = iterlast.cur;
    }
    if (prev) {
        new->next = prev->next;
        prev->next = new;
    } else {
        if (l->first)
            new->next = l->first;
        l->first = new;
    }
    return new;
}
void ackPush(AckList *l, Acknowledgment *ack) {
    AckListMem *m = l->mem;
    if (m->occupied >= ACKLIST_ENTRIES)
        errExit("Unable to add acknowledgment to the list (full).", NULL);
    m->occupied++;
    debugp("<?> ack push (%d)\n", m->occupied);
    for (int i=0; i<ACKLIST_ENTRIES; i++) {
        Acknowledgment *slot = &(m->acks)[i];
        debugp("<?> ack[%d] = (%d: %d→%d)\n", i, slot->message_id, slot->pid_sender, slot->pid_receiver);
        if (MSGSLOT_IS_FREE(slot)) {
            *slot = *ack;
            break;
        }
    }
}
void ackPop(AckList *l, int slot) {
    AckListMem *m = l->mem;
    Acknowledgment *as = &(m->acks)[slot];
    if (!MSGSLOT_IS_FREE(as)) {
        Acknowledgment zero = {};
        *as = zero;
        //as->pid_sender = 0;
        m->occupied--;
    }
}
char ackIterMessages(AckList *al, List *l) {
    if (!l->first) { //first call
        AckListMem *m = al->mem;
        //take unique message IDs
        for (int i=0; i<ACKLIST_ENTRIES; i++) {
            Acknowledgment *slot = &(m->acks)[i];
            if (MSGSLOT_IS_FREE(slot))
                continue;
            //search if the list does not already have the value
            Entry *cur = l->first;
            Entry *last = NULL;
            char found = 0;
            while (!found && cur) {
                found = (*(int*)cur->value) == slot->message_id;
                last = cur;
                cur = cur->next;
            }
            if (found)
                continue;
            last = appendList(l, last, sizeof(int));
            *(int*)last->value = slot->message_id;
        }
        l->cur = NULL;
    }
    return iterateList(l, 1);
}
char ackIterAcks(AckList *al, int message_id, List *l) {
    if (!l->first) { //first call
        AckListMem *m = al->mem;
        Entry *last = NULL;
        debugp("<?> iterating acks (%d) with id=%d\n", m->occupied, message_id);
        //filter acks by message IDs
        for (int i=0; i<ACKLIST_ENTRIES; i++) {
            Acknowledgment *slot = &(m->acks)[i];
            if (MSGSLOT_IS_FREE(slot) || slot->message_id != message_id)
                continue;
            last = appendList(l, last, sizeof(int));
            *(int*)last->value = i;
        }
        l->cur = NULL;
    }
    return iterateList(l, 1);
}
// END


// BEGIN Specifico
void getFIFOPath(string out, pid_t pid) {
    sprintf(out, "/tmp/dev_fifo.%d", pid);
}
void getOutputPath(string out, int msg_id) {
    sprintf(out, "./out_%d.txt", msg_id);
}
void sendMessageExternal(Message *msg, pid_t sender, pid_t receiver, char ping) {
    pid_t oldsender = msg->pid_sender;
    pid_t oldreceiver = msg->pid_receiver;
    msg->pid_sender = sender;
    msg->pid_receiver = receiver;
    debugp("<?> receiver=%d\n", receiver);

    char fifo_dest[PATH_LEN] = {};
    getFIFOPath(fifo_dest, receiver);
    int fd = openWrite(fifo_dest, 0);
    ssize_t amount = write(fd, msg, MSGSIZE);
    msg->pid_sender = oldsender;
    msg->pid_receiver = oldreceiver;

    if (amount < MSGSIZE)
        errExit("Incomplete message write to fifo.\n", NULL);

    if (ping)
        kill(receiver, SIGUSR1);
    closeFile(fd);
}
// END
