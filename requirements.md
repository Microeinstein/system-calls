# Requirements

* 5 dispositivi mobili:
    * **fifo** `/tmp/dev_fifo.$PID` in lettura
    * lista messaggi da inviare
    * ricevono da:
        * più devices vicini
        * più clients
    * operazioni:
        * invio messaggi ricevuti:
            1. blocca semaforo scacchiera && acklist
            1. individuare altri device che non abbiano già ricevuto il messaggio
                1. se il device è nel raggio (distanza Euclidea su `max_dist`):
                    1. invia messaggio (via **fifo**) e interrompi ricerca
            1. (messaggio non inviato) se sono stati trovati device:
                1. mantieni il messaggio nella lista
            1. sblocca acklist && semaforo scacchiera

        * ricezione messaggi da vicini:
            1. segna acknowledgement su **memoria condivisa**:
                * `pid_sender`
                * `pid_receiver`
                * `message_id`
                * `date_time` (tempo ricezione)
            1. aggiungi alla lista messaggi

* ack_manager:
    * inizializza coda messaggi `msg_queue` tramite `msg_queue_key`
    * gestisce lista acknowledgment _(100 max)_
    * ogni 5 secondi:
        * per ogni messaggio:
            * se tutti i dispositivi lo hanno ricevuto:
                * ottieni lista acknowledgment del messaggio
                * invia lista al client mittente tramite **coda messaggi** `msg_queue`
                * marca come liberi gli elementi nella lista _(`free()` ?)_

* scacchiera:
    * **memoria condivisa**
    * 10x10
    * cella -> pid device oppure 0
    * ↑ ←

* client:
    * si connette alla **coda messaggi** `msg_queue`
    * si può connettere ad un device tramite **fifo** `dev_fifo.$PID`
    * passa un messaggio:
        * `pid_sender`   (client)
        * `pid_receiver` (dev. ricevente)
        * `message_id`
        * `message`      (testo)
        * `max_dist`     (double >= 0)
    * quando riceve un messaggio tramite `msg_queue`:
        * genera un file `out_${MESSAGE_ID}.txt`
        * scrive i 5 acknowledgment
        * termina.

* server:
    * genera:
        * processi ack_manager e devices tramite `fork()`
        * segmenti **memoria condivisa**:
            * scacchiera
            * lista acknowledgment
        * **semafori**:
            * accesso ai segmenti di memoria condivisa
            * movimento dei device
    * ogni 2 secondi:
        * blocca semaforo scacchiera
        * stampa
            ```c
            printf("# Step %05d: device positions ###############\n", i)
            ```
        * muovi devices 1 -> 5:
            * in ordine
            * spostamenti sincronizzati:
                * **semafori** `SEM_IDX_BOARD`
                * altro
            * stampa
                ```c
                printf("%d %d %d msgs: %s\n", pid_device, dev.i, dev.j, lista_message_id)
                ```
            * muovi device
        * stampa
            ```c
            printf("##############################################\n\n")
            ```
        * sblocca semaforo scacchiera
    * termina (solo se riceve SIGTERM):
        1. termina tutti i device
        1. termina ack_manager
        1. chiusura **meccanismi sincronizzazione e comunicazione**
    * ogni altro segnale _non necessario_ dev'essere bloccato


## Summary

* Numero processi: 7+c
* Aree memoria condivisa: 2 (scacchiera, gestione ack)
* Numero semafori: 7 (scacchiera, lista ack, movimenti dei device)


## Running

1. Server: `./server msg_queue_key file_posizioni`
    * `msg_queue_key` - chiave della coda dei messaggi
    * `file_posizioni` - percorso file posizioni

1. Client: `./client msg_queue_key`
    * `msg_queue_key` - chiave della coda dei messaggi
    1. inserimento interattivo:
        * `pid_device`
        * `message_id`
        * `message`
        * `max_dist`
    1. apertura fifo device
    1. invio messaggio
    1. chiusura fifo device
    1. attesa lista ack
    1. stampa lista su file
    1. termina.


## Main structures

```c
typedef struct {
    pid_t  pid_sender;
    pid_t  pid_receiver;
    int    message_id;
    char   message[256];
    double max_distance;
} Message;

typedef struct {
    pid_t  pid_sender;
    pid_t  pid_receiver;
    int    message_id;
    time_t timestamp;
} Acknowledgment;
```

File posizioni:
```
dv1|dv2|dv3|dv4|dv5
```
```
x,y|x,y|x,y|x,y|x,y
x,y|x,y|x,y|x,y|x,y
x,y|x,y|x,y|x,y|x,y
...
```

`out_${MESSAGE_ID}.txt`:
```
Messaggio '$MESSAGE_ID': '$MESSAGE'
Lista acknowledgment:
$PID_CLIENT, $PID_D1, $DATE_TIME
$PID_D1,     $PID_D2, $DATE_TIME
$PID_D2,     $PID_D3, $DATE_TIME
$PID_D3,     $PID_D4, $DATE_TIME
$PID_D4,     $PID_D5, $DATE_TIME
```


## Info

* Vietato: funzioni C gestione filesystem (`fopen`, ...)
* Piattaforme: Ubuntu 18, REPL
* Compilazione: `make`
* Consegna: (creato con `tar`)

    ${MATRICOLA}_sistemi_operativi.tar.gz:
        /sistemi_operativi
            /system-call
            /MentOS/scheduler_algorithm.c
            /MentOS/buddysystem.c
