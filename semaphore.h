/// @file semaphore.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione dei SEMAFORI.

#pragma once
#include <sys/stat.h>
#include <sys/sem.h>

typedef int semSet;
typedef unsigned short semNum;
typedef unsigned short semVal;
typedef short semOp;

typedef union {
    int             val;
    struct semid_ds *buf;
    semVal          *array;
} semun;

typedef struct {
    semSet set;
    semNum num;
} Semaphore;


semSet semCreateSet(int size);
void semInit(semSet set, semVal *array);
void semExec(semSet set, semNum num, semOp op);
void semRemoveSet(semSet set);
void semP(Semaphore *s);
void semV(Semaphore *s);
