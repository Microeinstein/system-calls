/// @file signals.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione dei SEGNALI.

#include <stdio.h>
#include <signal.h>
#include "signals.h"

void sigFill(sigset_t *set) {
    if (sigfillset(set) fails)
        perrExit("Unable to fill signal set");
}

void sigExclude(sigset_t *set, sig num) {
    if (sigdelset(set, num) fails)
        perrExit("Unable to exclude signal from set");
}

void sigSetMask(sigset_t *set) {
    if (sigprocmask(SIG_SETMASK, set, NULL) fails)
        perrExit("Unable to set process signals mask");
}

void sigSetHandler(sig num, __sighandler_t handler) {
    if (signal(num, handler == NULL ? SIG_DFL : handler) == SIG_ERR)
        perrExit("Unable to set signal handler");
}
