/// @file err_exit.h
/// @brief Contiene la definizione della funzione di stampa degli errori.

#pragma once
#include "defines.h"

/// @brief Prints the error message of the last failed
///        system call and terminates the calling process.
void perrExit(const string fmt, ...);

/// @brief Prints the error message
///         and terminates the calling process.
void errExit(const string fmt, void (*func)(void), ...);
