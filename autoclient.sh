#!/bin/bash

autoinput() {
    pid="${1:?Missing device pid.}"
    msg="ayylmao_$pid"
    dist="8"
    shift 1
    
    printf "%s\n" "$pid" "$msg" "$dist" "$@"
}

autoinput "$@" | ./client 100
