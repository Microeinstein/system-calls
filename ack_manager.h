/// @file ack_manager.h
/// @brief Contiene le definizioni di ACK_MANAGER.

#pragma once
#include <sys/msg.h>
#include "defines.h"
#include "msg_queue.h"

#define CHECK_INTERVALL 5

typedef struct {
    char checking;
    DISPOSABLE(msq, queue);
} AckManagerGlobals;


void mainAckManager(key_t msg_queue_key);
